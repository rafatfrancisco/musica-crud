package br.com.torresti.musica.crud.model.entidy;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 * The persistent class for the discomusica database table.
 * 
 */
@Entity
@NamedQuery(name = "MusicaDisco.findAll", query = "SELECT d FROM MusicaDisco d")
public class MusicaDisco implements BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMusicaDisco;

	private String artista;

	private Integer categoria;

	private String musica;

	// bi-directional many-to-one association to Disco
	@ManyToOne
	@JoinColumn(name = "idDisco")
	private Disco disco;

	// bi-directional many-to-one association to TagDiscoMusica
	@OneToMany(mappedBy = "musicaDisco", fetch = FetchType.LAZY)
	private List<TagDiscoMusica> listaTagDiscoMusica;

	public MusicaDisco() {
	}

	public Integer getIdMusicaDisco() {
		return idMusicaDisco;
	}

	public void setIdMusicaDisco(Integer idMusicaDisco) {
		this.idMusicaDisco = idMusicaDisco;
	}

	public String getArtista() {
		return this.artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public Integer getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public String getMusica() {
		return this.musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}

	public Disco getDisco() {
		return this.disco;
	}

	public void setDisco(Disco disco) {
		this.disco = disco;
	}

	public List<TagDiscoMusica> getListaTagDiscoMusica() {
		return this.listaTagDiscoMusica;
	}

	public void setListaTagDiscoMusica(List<TagDiscoMusica> listaTagDiscoMusica) {
		this.listaTagDiscoMusica = listaTagDiscoMusica;
	}

	public TagDiscoMusica addListaTagDiscoMusica(TagDiscoMusica listaTagDiscoMusica) {
		getListaTagDiscoMusica().add(listaTagDiscoMusica);
		listaTagDiscoMusica.setMusicaDisco(this);

		return listaTagDiscoMusica;
	}

	public TagDiscoMusica removeListaTagDiscoMusica(TagDiscoMusica listaTagDiscoMusica) {
		getListaTagDiscoMusica().remove(listaTagDiscoMusica);
		listaTagDiscoMusica.setMusicaDisco(null);

		return listaTagDiscoMusica;
	}

}