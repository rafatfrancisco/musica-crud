package br.com.torresti.musica.crud.model.entidy;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


/**
 * The persistent class for the artistamusica database table.
 * 
 */
@Entity
@NamedQuery(name="ArtistaMusica.findAll", query="SELECT a FROM ArtistaMusica a")
public class ArtistaMusica implements BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idArtistaMusica;

	private String artista;

	private Integer categoria;

	private String musica;

	private Integer situacao;

	//bi-directional many-to-one association to Album
	@ManyToOne
	@JoinColumn(name="idAlbum")
	private Album album;

	//bi-directional many-to-one association to TagArtistaMusica
	@OneToMany(mappedBy="artistaMusica", fetch=FetchType.LAZY)
	private List<TagArtistaMusica> listaTagArtistaMusica;

	public ArtistaMusica() {
	}

	public Integer getIdArtistaMusica() {
		return this.idArtistaMusica;
	}

	public void setIdArtistaMusica(Integer idArtistaMusica) {
		this.idArtistaMusica = idArtistaMusica;
	}

	public String getArtista() {
		return this.artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public Integer getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public String getMusica() {
		return this.musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}

	public Integer getSituacao() {
		return this.situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Album getAlbum() {
		return this.album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public List<TagArtistaMusica> getListaTagArtistaMusica() {
		return this.listaTagArtistaMusica;
	}

	public void setListaTagArtistaMusica(List<TagArtistaMusica> listaTagArtistaMusica) {
		this.listaTagArtistaMusica = listaTagArtistaMusica;
	}

	public TagArtistaMusica addListaTagArtistaMusica(TagArtistaMusica listaTagArtistaMusica) {
		getListaTagArtistaMusica().add(listaTagArtistaMusica);
		listaTagArtistaMusica.setArtistaMusica(this);

		return listaTagArtistaMusica;
	}

	public TagArtistaMusica removeListaTagArtistaMusica(TagArtistaMusica listaTagArtistaMusica) {
		getListaTagArtistaMusica().remove(listaTagArtistaMusica);
		listaTagArtistaMusica.setArtistaMusica(null);

		return listaTagArtistaMusica;
	}

}