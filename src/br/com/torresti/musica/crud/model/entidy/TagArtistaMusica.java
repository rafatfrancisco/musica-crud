package br.com.torresti.musica.crud.model.entidy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the tagartistamusica database table.
 * 
 */
@Entity
@NamedQuery(name="TagArtistaMusica.findAll", query="SELECT t FROM TagArtistaMusica t")
public class TagArtistaMusica implements BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idTagArtistaMusica;

	//bi-directional many-to-one association to ArtistaMusica
	@ManyToOne
	@JoinColumn(name="idArtistaMusica")
	private ArtistaMusica artistaMusica;

	//bi-directional many-to-one association to Tag
	@ManyToOne
	@JoinColumn(name="idTag")
	private Tag tag;

	public TagArtistaMusica() {
	}

	public Integer getIdTagArtistaMusica() {
		return this.idTagArtistaMusica;
	}

	public void setIdTagArtistaMusica(Integer idTagArtistaMusica) {
		this.idTagArtistaMusica = idTagArtistaMusica;
	}

	public ArtistaMusica getArtistaMusica() {
		return this.artistaMusica;
	}

	public void setArtistaMusica(ArtistaMusica artistaMusica) {
		this.artistaMusica = artistaMusica;
	}

	public Tag getTag() {
		return this.tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

}