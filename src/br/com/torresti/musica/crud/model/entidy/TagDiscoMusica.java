package br.com.torresti.musica.crud.model.entidy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the tagdiscomusica database table.
 * 
 */
@Entity
@NamedQuery(name="TagDiscoMusica.findAll", query="SELECT t FROM TagDiscoMusica t")
public class TagDiscoMusica implements BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idTagDiscoMusica;

	//bi-directional many-to-one association to DiscoMusica
	@ManyToOne
	@JoinColumn(name="idDiscoMusica")
	private MusicaDisco musicaDisco;

	//bi-directional many-to-one association to Tag
	@ManyToOne
	@JoinColumn(name="idTag")
	private Tag tag;

	public TagDiscoMusica() {
	}

	public Integer getIdTagDiscoMusica() {
		return this.idTagDiscoMusica;
	}

	public void setIdTagDiscoMusica(Integer idTagDiscoMusica) {
		this.idTagDiscoMusica = idTagDiscoMusica;
	}

	public MusicaDisco getMusicaDisco() {
		return musicaDisco;
	}

	public void setMusicaDisco(MusicaDisco musicaDisco) {
		this.musicaDisco = musicaDisco;
	}

	public Tag getTag() {
		return this.tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

}