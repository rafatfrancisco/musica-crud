package br.com.torresti.musica.crud.model.entidy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the prateleira database table.
 * 
 */
@Entity
@NamedQuery(name = "Prateleira.findAll", query = "SELECT p FROM Prateleira p")
public class Prateleira implements BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPrateleira;

	private String nome;

	public Prateleira() {
	}

	public Integer getIdPrateleira() {
		return idPrateleira;
	}

	public void setIdPrateleira(Integer idPrateleira) {
		this.idPrateleira = idPrateleira;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}