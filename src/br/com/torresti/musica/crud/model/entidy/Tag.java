package br.com.torresti.musica.crud.model.entidy;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


/**
 * The persistent class for the tag database table.
 * 
 */
@Entity
@NamedQuery(name="Tag.findAll", query="SELECT t FROM Tag t")
public class Tag implements BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idTag;

	private String nome;

	//bi-directional many-to-one association to TagArtistaMusica
	@OneToMany(mappedBy="tag", fetch=FetchType.LAZY)
	private List<TagArtistaMusica> listaTagArtistaMusica;

	public Tag() {
	}

	public Integer getIdTag() {
		return this.idTag;
	}

	public void setIdTag(Integer idTag) {
		this.idTag = idTag;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<TagArtistaMusica> getListaTagArtistaMusica() {
		return this.listaTagArtistaMusica;
	}

	public void setListaTagArtistaMusica(List<TagArtistaMusica> listaTagArtistaMusica) {
		this.listaTagArtistaMusica = listaTagArtistaMusica;
	}

	public TagArtistaMusica addListaTagArtistaMusica(TagArtistaMusica listaTagArtistaMusica) {
		getListaTagArtistaMusica().add(listaTagArtistaMusica);
		listaTagArtistaMusica.setTag(this);

		return listaTagArtistaMusica;
	}

	public TagArtistaMusica removeListaTagArtistaMusica(TagArtistaMusica listaTagArtistaMusica) {
		getListaTagArtistaMusica().remove(listaTagArtistaMusica);
		listaTagArtistaMusica.setTag(null);

		return listaTagArtistaMusica;
	}

}