package br.com.torresti.musica.crud.model.entidy;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the album database table.
 * 
 */
@Entity
@NamedQuery(name="Album.findAll", query="SELECT a FROM Album a")
public class Album implements BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idAlbum;

	private Integer ano;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;

	private String nome;

	private String selo;

	private Integer tipo;

	private String urlSite;

	//bi-directional many-to-one association to ArtistaMusica
	@OneToMany(mappedBy="album", fetch=FetchType.LAZY)
	private List<ArtistaMusica> listaArtistaMusica;

	public Album() {
	}

	public Integer getIdAlbum() {
		return this.idAlbum;
	}

	public void setIdAlbum(Integer idAlbum) {
		this.idAlbum = idAlbum;
	}

	public Integer getAno() {
		return this.ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSelo() {
		return this.selo;
	}

	public void setSelo(String selo) {
		this.selo = selo;
	}

	public Integer getTipo() {
		return this.tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getUrlSite() {
		return this.urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public List<ArtistaMusica> getListaArtistaMusica() {
		return this.listaArtistaMusica;
	}

	public void setListaArtistaMusica(List<ArtistaMusica> listaArtistaMusica) {
		this.listaArtistaMusica = listaArtistaMusica;
	}

	public ArtistaMusica addListaArtistaMusica(ArtistaMusica listaArtistaMusica) {
		getListaArtistaMusica().add(listaArtistaMusica);
		listaArtistaMusica.setAlbum(this);

		return listaArtistaMusica;
	}

	public ArtistaMusica removeListaArtistaMusica(ArtistaMusica listaArtistaMusica) {
		getListaArtistaMusica().remove(listaArtistaMusica);
		listaArtistaMusica.setAlbum(null);

		return listaArtistaMusica;
	}

}