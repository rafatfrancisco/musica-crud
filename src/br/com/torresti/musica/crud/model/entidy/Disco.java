package br.com.torresti.musica.crud.model.entidy;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the disco database table.
 * 
 */
@Entity
@NamedQuery(name = "Disco.findAll", query = "SELECT d FROM Disco d")
public class Disco implements BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDisco;

	private Integer ano;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;

	private String nome;

	private String selo;

	private Integer tipo;

	private String urlSite;

	private Integer situacao;

	private String urlImagem;
	
	private String codigo;

	// bi-directional many-to-one association to Tag
	@ManyToOne
	@JoinColumn(name = "idPrateleira")
	private Prateleira prateleira;

	// bi-directional many-to-one association to DiscoMusica
	@OneToMany(mappedBy = "disco", fetch = FetchType.LAZY)
	private List<MusicaDisco> listaDiscoMusica;

	public Disco() {
	}

	public Integer getIdDisco() {
		return this.idDisco;
	}

	public void setIdDisco(Integer idDisco) {
		this.idDisco = idDisco;
	}

	public Integer getAno() {
		return this.ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSelo() {
		return this.selo;
	}

	public void setSelo(String selo) {
		this.selo = selo;
	}

	public Integer getTipo() {
		return this.tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getUrlSite() {
		return this.urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Prateleira getPrateleira() {
		return prateleira;
	}

	public void setPrateleira(Prateleira prateleira) {
		this.prateleira = prateleira;
	}

	public List<MusicaDisco> getListaDiscoMusica() {
		return this.listaDiscoMusica;
	}

	public void setListaDiscoMusica(List<MusicaDisco> listaDiscoMusica) {
		this.listaDiscoMusica = listaDiscoMusica;
	}

	public MusicaDisco addListaDiscoMusica(MusicaDisco listaDiscoMusica) {
		getListaDiscoMusica().add(listaDiscoMusica);
		listaDiscoMusica.setDisco(this);

		return listaDiscoMusica;
	}

	public MusicaDisco removeListaDiscoMusica(MusicaDisco listaDiscoMusica) {
		getListaDiscoMusica().remove(listaDiscoMusica);
		listaDiscoMusica.setDisco(null);

		return listaDiscoMusica;
	}

}